"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""

import os
import flask
from flask import redirect, url_for, request, render_template, abort, Response, make_response
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
from pymongo import MongoClient
from flask_restful import Resource, Api
import logging
import csv
import io

###
# Globals
###
app = flask.Flask(__name__)
api = Api(app)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

# Step 1: create a client object
# The environment variable DB_PORT_27017_TCP_ADDR is the IP of a linked docker
# container (IP of the database). Generally "DB_PORT_27017_TCP_ADDR" can be set to
# "localhost" or "127.0.0.1".
client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)

# Step 2: connect to the DB
db = client.tododb



def submit(f):
    opens = []
    closes = []
    kms = []
    items = []
    for key in f.keys():
        for value in f.getlist(key):
            if (key == "open" and value != ""):
                opens.append(value)
            elif (key == "close" and value != ""):
                closes.append(value)
            elif (key == "km" and value != ""):
                kms.append(value)

    while (len(opens) > 0):
        items.append({
            'open': opens.pop(),
            'close': closes.pop(),
            'km': kms.pop()
        })
    return items

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404



###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    brevet_km = request.args.get('brevet_km', type=float)
    start_time = arrow.get(request.args.get('start_time', type=str))
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    # Updated these to use values from the page
    open_time = acp_times.open_time(km, brevet_km, start_time.isoformat())
    close_time = acp_times.close_time(km, brevet_km, start_time.isoformat())
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)

@app.route('/_submit', methods=['POST'])
def new():
    f = request.form
    items = submit(f)
    while (len(items) > 0):
        db.tododb.insert_one(items.pop())
    return ('', 204)

@app.route('/_display', methods=['POST'])
def display():
    _items = db.tododb.find()
    items = [item for item in _items]
    if (len(items) == 0):
        return flask.render_template('empty.html')
    else:
        return render_template('todo.html', items=items)
#############
# RESTful api
#############
class ListAll(Resource):
    def get(self):
        _items = db.tododb.find()
        items = [item for item in _items]
        for id in items:
            del id['_id']
            del id['km']
        return items

class ListAllType(Resource):
    def get(self, type):
        _items = db.tododb.find()
        items = [item for item in _items]
        for id in items:
            del id['_id']
            del id['km']
        if (type == "csv"):
            csvTimes = io.StringIO()
            writer = csv.writer(csvTimes)
            writer.writerow(["open","close"])
            for times in items:
                writer.writerow([times['open'],times['close']])
            output = make_response(csvTimes.getvalue())
            output.headers["Content-Disposition"] = "attachment; filename=export.csv"
            output.headers["Content-type"] = "text/csv"
            return output
        elif (type == "json"):
            return items
        return flask.render_template('404.html'), 404

class ListOpen(Resource):
    def get(self):
        try:
            numResults = int(request.args.get("top"))
        except TypeError:
            numResults = 0
        _items = db.tododb.find()
        items = [item for item in _items]
        for id in items:
            del id['_id']
            del id['km']
            del id['close']
            id['sort'] = arrow.get(id['open'], 'ddd M/D H:mm').isoformat()
        if (numResults):
            # Shoutout to Mario F on stack exchange for this line on sort a list of dicts
            # https://stackoverflow.com/questions/72899/how-do-i-sort-a-list-of-dictionaries-by-a-value-of-the-dictionary
            items = sorted(items, key=lambda k: k['sort'])
            del items[numResults:]
        for id in items:
            del id['sort']
        return items

class ListOpenType(Resource):
    def get(self, type):
        try:
            numResults = int(request.args.get("top"))
        except TypeError:
            numResults = 0
        _items = db.tododb.find()
        items = [item for item in _items]
        for id in items:
            del id['_id']
            del id['km']
            del id['close']
            id['sort'] = arrow.get(id['open'], 'ddd M/D H:mm').isoformat()
        if (numResults):
            items = sorted(items, key=lambda k: k['sort'])
            del items[numResults:]
        for id in items:
            del id['sort']
        if (type == "csv"):
            csvTimes = io.StringIO()
            writer = csv.writer(csvTimes)
            writer.writerow(["open"])
            for times in items:
                writer.writerow([times['open']])
            output = make_response(csvTimes.getvalue())
            output.headers["Content-Disposition"] = "attachment; filename=export.csv"
            output.headers["Content-type"] = "text/csv"
            return output
        elif (type == "json"):
            return items
        return items

class ListClose(Resource):
    def get(self):
        try:
            numResults = int(request.args.get("top"))
        except TypeError:
            numResults = 0
        _items = db.tododb.find()
        items = [item for item in _items]
        for id in items:
            del id['_id']
            del id['km']
            del id['open']
            id['sort'] = arrow.get(id['close'], 'ddd M/D H:mm').isoformat()
        if (numResults):
            items = sorted(items, key=lambda k: k['sort'])
            del items[numResults:]
        for id in items:
            del id['sort']
        return items

class ListCloseType(Resource):
    def get(self, type):
        try:
            numResults = int(request.args.get("top"))
        except TypeError:
            numResults = 0
        _items = db.tododb.find()
        items = [item for item in _items]
        for id in items:
            del id['_id']
            del id['km']
            del id['open']
            id['sort'] = arrow.get(id['close'], 'ddd M/D H:mm').isoformat()
        if (numResults):
            items = sorted(items, key=lambda k: k['sort'])
            del items[numResults:]
        for id in items:
            del id['sort']
        if (type == "csv"):
            csvTimes = io.StringIO()
            writer = csv.writer(csvTimes)
            writer.writerow(["close"])
            for times in items:
                writer.writerow([times['close']])
            output = make_response(csvTimes.getvalue())
            output.headers["Content-Disposition"] = "attachment; filename=export.csv"
            output.headers["Content-type"] = "text/csv"
            return output
        elif (type == "json"):
            return items
        return items

class AddTimes(Resource):
    def post(self):
        items = [{'open': 'Sun 1/1 1:28', 'close': 'Sun 1/1 3:30', 'km': '50'}, {'open': 'Sun 1/1 1:11', 'close': 'Sun 1/1 3:00', 'km': '50'}, {'open': 'Sun 1/1 0:53', 'close': 'Sun 1/1 2:30', 'km': '30'}, {'open': 'Sun 1/1 0:35', 'close': 'Sun 1/1 2:00', 'km': '20'}, {'open': 'Sun 1/1 0:18', 'close': 'Sun 1/1 1:30', 'km': '10'}]
        while (len(items) > 0):
            db.tododb.insert_one(items.pop())
        return ('', 204)


api.add_resource(ListAll, '/listAll/')
api.add_resource(ListAllType, '/listAll/<type>')
api.add_resource(ListOpen, '/listOpenOnly')
api.add_resource(ListOpenType, '/listOpenOnly/<type>')
api.add_resource(ListClose, '/listCloseOnly')
api.add_resource(ListCloseType, '/listCloseOnly/<type>')
api.add_resource(AddTimes, '/addTimes')



app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
