import os
from flask import Flask, redirect, url_for, request, render_template
from pymongo import MongoClient

app = Flask(__name__)

# Step 1: create a client object
# The environment variable DB_PORT_27017_TCP_ADDR is the IP of a linked docker
# container (IP of the database). Generally "DB_PORT_27017_TCP_ADDR" can be set to
# "localhost" or "127.0.0.1".
client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)

# Step 2: connect to the DB
db = client.tododb
times = db.times

@app.route('/')
def todo():
    _items = db.tododb.find()
    items = [item for item in _items]

    return render_template('todo.html', items=items)

# https://www.diffen.com/difference/GET-vs-POST-HTTP-Requests
@app.route('/_submit', methods=['POST'])
def new():
    item_doc = {
        'open': request.form['open'],
        'close': request.form['close']
    }
    times.insert_one(item_doc)

    return redirect(url_for('todo'))

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
