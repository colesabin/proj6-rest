<html>
    <head>
        <title>Controle Times</title>
    </head>

    <body>
        <h1>List of all open and close times.</h1>
        <h3>This app needs times in the database!</h3>
        <p>You can visit   <a href="http://0.0.0.0:5000">the calculator</a> to add your own times, or use this button to add 5 premade times to the db.
        <br />Note: You can hit this button as many times as you like, but it will add the same 5 times over and over.
        <form role="form" method="POST" action="http://0.0.0.0:5000/addTimes">
        <input id="submit" type="submit" name="submit" value="Add Times" />
      </form></p>
        <ul>
            <?php
            $json = file_get_contents('http://api/listAll/');
            $obj = json_decode($json);
            foreach ($obj as $l) {
                echo "<li>Open: $l->open Close: $l->close</li>";
            }
            ?>
        </ul>
    </body>
</html>
