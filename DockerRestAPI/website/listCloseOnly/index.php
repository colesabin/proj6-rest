<html>
    <head>
        <title>Controle Times</title>
    </head>

    <body>
        <h1>List of all close times.</h1>
        <h3>This app needs times in the database!</h3>
        <p>You can visit   <a href="http://0.0.0.0:5000">the calculator</a> to add your own times, or use this button to add 5 premade times to the db.
        <br />Note: You can hit this button as many times as you like, but it will add the same 5 times over and over.
        <form role="form" method="POST" action="http://0.0.0.0:5000/addTimes">
        <input id="submit" type="submit" name="submit" value="Add Times" />
      </form></p>
        <p> Too many times? Enter the number of results you want below. </p>
        <form name="form" action="" method="get">
          <input type="text" name="top" id="top" value="">
          <input type="submit" value="Submit">
        </form>
        <ul>
            <?php
            $numResults = htmlspecialchars($_GET["top"]);
            if ($numResults != ""){
              $json = file_get_contents('http://api/listCloseOnly?top=' . $numResults);
            } else {
              $json = file_get_contents('http://api/listCloseOnly');
            }
            $obj = json_decode($json);
            foreach ($obj as $l) {
              echo "<li>Close: $l->close</li>";
            }
            ?>
        </ul>
    </body>
</html>
