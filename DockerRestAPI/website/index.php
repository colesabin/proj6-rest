<html>
    <head>
        <title>Times</title>
    </head>

    <body>
        <h1>Controle Time Consumer App</h1>
        <h3>This app needs times in the database!</h3>
        <p>You can visit   <a href="http://0.0.0.0:5000">the calculator</a> to add your own times, or use this button to add 5 premade times to the db.
        <br />Note: You can hit this button as many times as you like, but it will add the same 5 times over and over.
        <form role="form" method="POST" action="http://0.0.0.0:5000/addTimes">
        <input id="submit" type="submit" name="submit" value="Add Times" />
      </form></p>
        <a href="/listAll">View All Open and Close Times in the Database</a><br /><br />
        <a href="/listOpenOnly">View All Open Times in the Database</a><br /><br />
        <p> Too many times? Enter the number of results you want below. </p>
        <form name="form" action="/listOpenOnly" method="get">
          <input type="text" name="top" id="top" value="">
          <input type="submit" value="Submit">
        </form>

        <a href="/listCloseOnly">View All Close Times in the Database</a><br /><br />
        <p> Too many times? Enter the number of results you want below. </p>
        <form name="form" action="/listCloseOnly" method="get">
          <input type="text" name="top" id="top" value="">
          <input type="submit" value="Submit">
        </form>
    </body>
</html>
