# ACP Controle Time Calculator and RESTful API.



This app calculates the open and closing times for controle points in a brevet. A detailed explanation of the terms can be found on the [Randonneurs USA FAQ page](https://rusa.org/pages/rusa-faq), but the short version is that a brevet is a kind of race and the controle points are checkpoints that participants must pass through to finish the race. These controles are only open for specific windows of time, which is based largely on how far along in the race they are found.
The previous update version allowed you to save open and closing times, and then display those saved values on a new page. This newest version now exposes those times using a RESTful API. Additionally there is a PHP web app that uses this API to display times stored in the database.

## Authors:
---------------

### Original Version:
* **Michal Young**

### Updated Version:
* **R Durairajan** [proj6-rest](https://bitbucket.org/UOCIS322/proj6-rest/)

### Updates to creat RESTful API and PHP app:

* **Cole Sabin** [E-Mail](mailto:csabin@uoregon.edu)
## Instructions:
---------------

* Download the repo and update the credentials-skel.ini file with a secret key. Save as credentials.ini in the api folder.

* Build and run the app using the docker-compose.yml file in the DockerRestAPI folder.
  * The run.sh script is handy for this.

* Visit 0.0.0.0:5000 to access the largely unchanged calculator.
  * This is also the host:port address the API uses.

* Visit 0.0.0.0:5001 to use the new PHP app and view times in the database.

### API Details

* There are 3 basic ways to use the API:
  * 0.0.0.0:5000/listAll will return all open and close times in the database.
  * 0.0.0.0:5000/listOpenOnly will return all open times in the database.
  * 0.0.0.0:5000/listCloseOnly will return all close times in the database.

* Each of the above can be represented by either JSON(the default) or CSV formats.
  * Append /json to the end of the URL for JSON
    * Example: 0.0.0.0:5000/listAll/json will return all times in JSON format.
  * Append /csv to the end of the URL for CSV
      * Example: 0.0.0.0:5000/listCloseOnly/csv will return close times in CSV format.

* /listOpenOnly and /listCloseOnly can add a query parameter to get the top "k" open or close times
  * Append ?top=k to the end to retrieve the top "k" times.
    * Example:  0.0.0.0:5000/listCloseOnly?top=5 will return the top 5 close times in ascending order.
  * This can also be used with /json or /csv
    * Example 0.0.0.0:5000/listOpenOnly/csv?top=10 will return the top 10 open times in ascending order and csv format.

### Prerequisties:

* [Docker](http://www.docker.com) is required to use the Dockerfile to build the image and run the container.
	* If you don't have docker, or don't know how to use it, help can be found on the [Docker Get Started](https://docs.docker.com/get-started/) guide.

* The included tests use [nose](https://nose.readthedocs.io/en/latest/) and require python 3 to work correctly.
  * They also only work if you run nosetests from inside a running container...

## Calculation Details:
---------------

The algorithm for calculating the open and close times is almost entirely based on the [page explaining RUSA's own calculator.](https://rusa.org/pages/acp-brevet-control-times-calculator) A few additional details were taken from their [page of rules for organizers.](https://rusa.org/pages/orgreg)

The basic idea is that for any given controle point there will be an opening and closing time, creating a window of time that racers need to pass through the controle point in order to complete the brevet. These times are based on how far along that controle point is in the race, with a maximum km/hr for the opening and a minimum km/hr for the closing time. From the [rusa.org page:](https://rusa.org/pages/acp-brevet-control-times-calculator):

> For example, a distance of 100 km divided by a speed of 15 km per hour results in a time of
> 6.666666... hours. To convert that figure into hours and minutes, subtract the whole number
> of hours (6) and multiply the resulting fractional part by 60.
> The result is 6 hours, 40 minutes

The [table from RUSA](https://rusa.org/pages/acp-brevet-control-times-calculator) defining these max and min rates is as follows:

| Controle Location (km)    | Minimum Speed (km/hr)    | Maximum Speed (km/hr) |
| --------|---------|-------|
| 0 - 200  | 15   | 34    |
| 200 - 400 | 15   | 32    |
| 400 - 600  | 15   | 30    |
| 600 - 1000  | 11.428   | 28    |
| 1000 - 1300 | 13.333   | 26    |

The way these speeds are used is not immediately intuitive. For calculating an opening time for a controle point at 300km both the first and second rows of the above table are used. The first 200 km are at a rate of 34 km/hr and the last 100 are at the second rate of 32 km/hr.

Additionally in the Oddities section of that [page](https://rusa.org/pages/acp-brevet-control-times-calculator) it notes that the closing time for controles at 60 km or less are calculated at a rate of 20 km/hr, plus an additional hour. From that page:

> A control at 20 km would close at 20/20 + 1H00 = 2H00.
> A control at 60 km would close at 60/20 + 1H00 = 4H00
> which is exactly what the standard rule would calculate (60/15 = 4H00).
> Beyond 60km, the standard algorithm applies.

The closing time of the first controle is simply 1 hour after the opening time and the closing time of the final controle is also calculated differently. From the [page of rules for organizers.](https://rusa.org/pages/orgreg) :

> The closing time for the finish checkpoint is calculated by adding
> the maximum permitted time for the brevet to the opening time of
> the start checkpoint. Maximum permitted times (in hours and minutes, HH:MM)
> are 13:30 for 200 KM, 20:00 for 300 KM, 27:00 for 400 KM, 40:00 for 600 KM, and 75:00 for 1000 KM.


### Controle Point Correctness:

This calculator does very little to ensure that controle points are chosen correctly. Rules regarding choosing controle points can be found on the [RUSA page of rules for organizers.](https://rusa.org/pages/orgreg). The only exception to this is if your controle points are more than 20% beyond the length of the entire brevet a warning message will be displayed.
